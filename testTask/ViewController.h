//
//  ViewController.h
//  testTask
//
//  Created by hotdog on 11/18/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : BaseViewController

-(void) showDetailedControllerWithCity:(CityEntity*) city;

@end

