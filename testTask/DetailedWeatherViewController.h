//
//  DetailedWeatherViewController.h
//  testTask
//
//  Created by hotdog on 11/23/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailedWeatherViewController : BaseViewController

@property (nonatomic, strong) CityEntity *city;

@end
