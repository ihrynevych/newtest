//
//  NSMutableSet+disk.m
//  testTask
//
//  Created by Admin on 11/23/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "NSMutableSet+disk.h"

static NSString *const kSetHelperPath = @"set.file";

@implementation NSMutableSet (disk)

-(void) save {
    [self.allObjects writeToFile:[NSMutableSet _getPath] atomically:YES];
}

+(NSString*) _getPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    return ([paths count] > 0)?  [[paths objectAtIndex:0] stringByAppendingPathComponent:kSetHelperPath] : nil;
}

+(NSMutableSet*) load {
    NSArray *arrayFromFile = [NSArray arrayWithContentsOfFile:[NSMutableSet _getPath]];
    return arrayFromFile ? [NSMutableSet setWithArray:arrayFromFile] : [NSMutableSet new];
}
@end
