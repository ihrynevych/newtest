//
//  NSMutableSet+disk.h
//  testTask
//
//  Created by Admin on 11/23/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableSet (disk)

-(void) save;
+(NSMutableSet*) load;

@end
