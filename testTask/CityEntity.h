//
//  CityEntity.h
//  testTask
//
//  Created by hotdog on 11/18/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

@import UIKit;
#import <Foundation/Foundation.h>

typedef void(^CityEntityUpdetedBlock)();

typedef struct CityCoordinates {
    CGFloat lat;
    CGFloat lon;
} CityCoordinates;

CG_INLINE CityCoordinates
CityCoordinatesMake(CGFloat lat, CGFloat lon)
{
    CityCoordinates coordinates;
    coordinates.lat = lat;
    coordinates.lon = lon;
    return coordinates;
}

@interface BaseObject : NSObject

-(id) initWithJsonObject :(id) json;
-(void) fillObjectWithJson:(NSDictionary*) json;
@property (nonatomic, strong) NSDictionary *json;

@end

@interface Weather : BaseObject

@property (nonatomic, strong) NSString *weatherDescription;
@property (nonatomic, strong) NSString *iconUrl;
@property (nonatomic, strong) UIImage *iconImage;

@end

@interface Clouds : BaseObject

@property (nonatomic, strong) NSNumber *all;

@end

@interface Main : BaseObject

@property (nonatomic, strong) NSString *groundLevel;
@property (nonatomic, strong) NSNumber *humidity;
@property (nonatomic, strong) NSString *pressure;
@property (nonatomic, strong) NSString *seaLevel;
@property (nonatomic, strong) NSNumber *currentTemperature;
@property (nonatomic, strong) NSNumber *maxTemperature;
@property (nonatomic, strong) NSNumber *minTemperature;


@end

@interface  Coordinates : BaseObject

@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longtitude;

@end

@interface Wind : BaseObject

@property (nonatomic, strong) NSNumber *degres;
@property (nonatomic, strong) NSNumber *speed;

@end

@interface CityEntity : BaseObject

-(void) updateWithFinishBlock:(CityEntityUpdetedBlock) block;

@property (nonatomic, strong) Clouds *clouds;
@property (nonatomic, strong) NSNumber *code;
@property (nonatomic, strong) Coordinates *coordinates2D;
@property (nonatomic, strong) Main *mainProperties;
@property (nonatomic, strong) Weather *weather;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) Wind *wind;


@end
