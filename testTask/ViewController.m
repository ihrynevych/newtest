//
//  ViewController.m
//  testTask
//
//  Created by hotdog on 11/18/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "ViewController.h"
#import "GeneralWeatherCell.h"
#import "GeneralAutocompleteCityCell.h"
#import "DetailedWeatherViewController.h"

static NSString *const kDetailedViewControllerSegue = @"cityDetailedViewControllerSegueId";
static NSString *const kCityAddedMessageFormater = @"place %@ added!"; //%@ - city name

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (nonatomic, strong) NSMutableArray *citiesObjects;
@property (nonatomic, strong) NSMutableArray *autocompleteObjects;
@property (nonatomic, assign) BOOL showAutocompleteCells;

@property (nonatomic, strong) CityEntity *selectedCity;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.cityTextField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    [self.cityTextField addTarget:self
                           action:@selector(textFieldDidTapped:)
                 forControlEvents:UIControlEventAllTouchEvents];
    [self _setupPullToRefresh];
}

-(void) _setupPullToRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(_updateData)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.doneButton.enabled = NO;
    self.showAutocompleteCells = NO;
    [self _prepareSelectedCitiesToShow];
    self.autocompleteObjects = [NSMutableArray new];
}

-(void) _updateData {
    [self.refreshControl endRefreshing];
    [self.citiesObjects enumerateObjectsUsingBlock:^(CityEntity *cityObject, NSUInteger idx, BOOL * _Nonnull stop) {
        [cityObject updateWithFinishBlock:^{
            if ([self _isVisibleRow:idx]) {
                [self.tableView beginUpdates];
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
            }
        }];
    }];
}

-(BOOL) _isVisibleRow:(NSUInteger) row {
    NSArray *indexes = [self.tableView indexPathsForVisibleRows];
    for (NSIndexPath *index in indexes) {
        if (index.row == row) {
            return YES;
        }
    }
    return NO;
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark user actions
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

- (IBAction) donePressed:(id)sender {
    self.cityTextField.text = @"";
    [self.cityTextField resignFirstResponder];
    self.showAutocompleteCells = NO;
    [self _prepareSelectedCitiesToShow];
    [self.tableView reloadData];
    self.doneButton.enabled = NO;
}

-(void) showDetailedControllerWithCity:(CityEntity*) city {
    self.selectedCity = city;
    [self performSegueWithIdentifier:kDetailedViewControllerSegue sender:self];
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark table view
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.showAutocompleteCells ? self.autocompleteObjects.count : self.citiesObjects.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.showAutocompleteCells) {
        [API_CLIEENT getInfoForPlace:self.autocompleteObjects[indexPath.row] withCompletionBlock:^(NSDictionary *city, NSError *error) {
            [API_CLIEENT addCity:city];
            [super showTemporaryAlertWithMessage:[NSString stringWithFormat:kCityAddedMessageFormater, city[@"city"][@"place"]]];
        }];
    } else {
        [self showDetailedControllerWithCity:(CityEntity*)self.citiesObjects[indexPath.row]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *simpleTableIdentifier = self.showAutocompleteCells ? @"GeneralAutocompleteCityCellId" : @"cityGeneralWeatherCellId";
    
    if (self.showAutocompleteCells) {
        GeneralAutocompleteCityCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.regionLabel.text = self.autocompleteObjects[indexPath.row][@"place"];
        return cell;
    } else {
        GeneralWeatherCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        CityEntity *currentCity = (CityEntity*)self.citiesObjects[indexPath.row];;
        cell.cityLabel.text = currentCity.city;
        [cell.icon setImageWithUrl:currentCity.weather.iconUrl];
        NSString* formattedTemperature = [NSString stringWithFormat:@"%.02f˚C", @((currentCity.mainProperties.currentTemperature.floatValue - F_CONSTANT) ).floatValue];
        cell.tempMaxLabel.text = formattedTemperature;
        cell.tempMinLabel.text = currentCity.mainProperties.humidity.stringValue;
        cell.windSpeedLabel.text = currentCity.wind.speed.stringValue;
        cell.descriptionLabel.text = currentCity.weather.weatherDescription;
        [cell rotateArrowToAngel:currentCity.wind.degres];
        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return !self.showAutocompleteCells;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        CityEntity *currentCity = (CityEntity*)self.citiesObjects[indexPath.row];
        [API_CLIEENT removeCityByName:currentCity.city];
        [self.citiesObjects removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark textField methods
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

-(void) textFieldDidTapped:(UITextField*) textField {
    self.showAutocompleteCells = YES;
    [self.autocompleteObjects removeAllObjects];
    [self.tableView reloadData];
    self.doneButton.enabled = YES;
}

-(void) textFieldDidChange:(UITextField*) textField {
    [self.citiesObjects removeAllObjects];
    weakRef(self);
    [self.tableView reloadData];
    [API_CLIEENT getPlacesForCity:textField.text withCompletionBlock:^(NSArray *json, NSError *error) {
        weakself.autocompleteObjects = [NSMutableArray arrayWithArray:json];
        [weakself.tableView reloadData];
    }];
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark working with segues
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kDetailedViewControllerSegue]) {
        DetailedWeatherViewController *destination = segue.destinationViewController;
        destination.city = self.selectedCity;
    }
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark private methods
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

-(void) _prepareSelectedCitiesToShow {
    self.citiesObjects = [NSMutableArray new];
    [self.tableView reloadData];
    weakRef(self);
    [API_CLIEENT.selectedCities enumerateObjectsUsingBlock:^(NSDictionary  *cityJSON, BOOL * _Nonnull stop) {
        [API_CLIEENT getWeatherFromGPlacesJson:cityJSON withCompletionBlock:^(id json, NSError *error) {
            CityEntity *city = [[CityEntity alloc] initWithJsonObject:json];
            city.json = cityJSON;
            [weakself.citiesObjects addObject:city];
            [weakself.tableView beginUpdates];
            [weakself.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.citiesObjects.count - 1 inSection:0]]
                                      withRowAnimation:UITableViewRowAnimationLeft];
            [weakself.tableView endUpdates];
        }];
    }];
}

@end
