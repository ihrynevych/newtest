//
//
//  Created by Ihor Hrynevych
//

#import "LLAPIClient.h"

@interface LLAPIClient ()

@end

@implementation LLAPIClient
//-------------------------------------------------------
#pragma mark -
#pragma mark singleton
#pragma mark -
//-------------------------------------------------------

+ (LLAPIClient *)sharedClient
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LLAPIClient alloc] init];
    });
    
    return sharedInstance;
}

- (id)copy {
    return self;
}

-(id) init {
    self = [super init];
    if (self) {
        self.selectedCities = [NSMutableSet load];
    }
    return self;
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark network methods
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

-(NSDictionary*) findCityByName:(NSString*) cityName {
    __block NSDictionary *searchingCity;
    [self.selectedCities enumerateObjectsUsingBlock:^(NSDictionary  *cityJSON, BOOL * _Nonnull stop) {
        if ([cityJSON[@"city"][@"place"] isEqualToString:cityName]) {
            searchingCity = cityJSON;
            *stop = YES;
        }
    }];
    return searchingCity;
}

-(void) removeCityByName:(NSString*) cityName {
    NSDictionary *city = [self findCityByName:cityName];
    if (city) {
        [self.selectedCities removeObject:city];
    }
}

-(void) getPlacesForCity:(NSString*) city withCompletionBlock:(JSONResponseBlock) block {
    NSString *urlString = [NSString stringWithFormat:kPlacesSearchUrl, city, CONFIGURATION_FOR_KEY(@"GPLACE_APP_KEY")];
    [self _processUrlString:urlString withCompletionBlock:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (block) {
            NSArray *places = [self _getParsedData:data][@"predictions"];
            NSMutableArray *result = [NSMutableArray new];
            [places enumerateObjectsUsingBlock:^(NSDictionary *place, NSUInteger idx, BOOL * _Nonnull stop) {
                NSDictionary *currentCity = @{@"place" : place[@"description"],
                                               @"placeId" : place[@"place_id"]};
                [result addObject:currentCity];
            }];
            block (result, error);
        }
    }];
}

-(void) getWeatherFromGPlacesJson:(NSDictionary*) cityJSON withCompletionBlock:(JSONResponseBlock) block {
    CityCoordinates coordinates = CityCoordinatesMake(((NSNumber*)cityJSON[@"lat"]).floatValue, ((NSNumber*)cityJSON[@"lng"]).floatValue);
    NSString *city = cityJSON[@"city"][@"place"];
    NSString *urlString = [NSString stringWithFormat:kOpenWeatherAPIKey, coordinates.lat, coordinates.lon, CONFIGURATION_FOR_KEY(@"WEATHER_APP_KEY")];
    [self _addCityToSpotlight:cityJSON];
    [self _processUrlString:urlString withCompletionBlock:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (block) {
            NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:[self _getParsedData:data]];
            [result setObject:city ? city : @"" forKey:@"city"];
            block (result, error);
        }
    }];
}

-(void) _addCityToSpotlight:(NSDictionary*) cityJson {
    
    CSSearchableItemAttributeSet *attributes = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:@"city"];
    attributes.title = cityJson[@"city"][@"place"];
    attributes.keywords = [cityJson[@"city"][@"place"] componentsSeparatedByString:@","];
    CSSearchableIndex *index = [CSSearchableIndex defaultSearchableIndex];
    CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:cityJson[@"city"][@"place"] domainIdentifier:@"com.testtask.ihor.cities" attributeSet:attributes];
    [index indexSearchableItems:@[item] completionHandler:^(NSError * _Nullable error) {
        
    }];
}

-(void) getInfoForPlace:(NSDictionary *) place withCompletionBlock:(JSONResponseBlock) block {
    NSString *urlString = [NSString stringWithFormat:kPlacesGetInfoByPlaceId, place[@"placeId"], CONFIGURATION_FOR_KEY(@"GPLACE_APP_KEY")];
    [self _processUrlString:urlString withCompletionBlock:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (block) {
            NSDictionary *response = [self _getParsedData:data];
            NSDictionary *geometry = @{@"lat" : response[@"result"][@"geometry"][@"location"][@"lat"] ,
                                       @"lng" : response[@"result"][@"geometry"][@"location"][@"lng"] ,
                                       @"city" : place ? place : @"" };
            block (geometry, error);
        }
    }];
}

-(void) _processUrlString:(NSString*) urlString withCompletionBlock:(URLSessionCompletionBlock) block {
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:block];
                                                        
                                       
    
    [dataTask resume];
}

-(NSDictionary*) _getParsedData:(NSData *) data {
    if (!data) {
        return nil;
    }
    NSError *error = nil;
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    return error ? nil : jsonDictionary;
}

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark objects methods
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

-(void) addCity:(NSDictionary*) city {
    if (city) {
        [_selectedCities addObject:city];
        [_selectedCities save];
    }
}

@end
