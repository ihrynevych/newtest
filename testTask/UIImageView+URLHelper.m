//
//  UIImageView+URLHelper.m
//  testTask
//
//  Created by hotdog on 11/19/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "UIImageView+URLHelper.h"

@implementation UIImageView (URLHelper)

-(void) setImageWithUrl:(NSString*) urlString {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *icon = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setImage:icon];
        });
    });
}

@end
