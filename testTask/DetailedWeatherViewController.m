//
//  DetailedWeatherViewController.m
//  testTask
//
//  Created by hotdog on 11/23/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "DetailedWeatherViewController.h"
@import MapKit;

@interface DetailedWeatherViewController ()

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *windDirectionLabel;

@end

@implementation DetailedWeatherViewController

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.cityLabel.text = self.city.city;
    NSString* formattedTemperature = [NSString stringWithFormat:@"%.02f˚C", @((self.city.mainProperties.currentTemperature.floatValue - F_CONSTANT) ).floatValue];
    self.temperatureLabel.text = formattedTemperature;
    self.windLabel.text = self.city.wind.speed.stringValue;
    self.windDirectionLabel.text = self.city.wind.degres.stringValue;
    self.humidityLabel.text = self.city.mainProperties.humidity.stringValue;
}

- (IBAction) mapPressed:(id)sender {
    CLLocationCoordinate2D coords ;
    coords.latitude = self.city.coordinates2D.latitude.doubleValue;
    coords.longitude = self.city.coordinates2D.longtitude.doubleValue;
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coords addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:self.city.city];
    [mapItem openInMapsWithLaunchOptions:nil];
}

@end
