//
//  GeneralAutocompleteCityCell.h
//  testTask
//
//  Created by Admin on 11/21/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralAutocompleteCityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *regionLabel;

@end
