//
//  GeneralWeatherCell.m
//  testTask
//
//  Created by hotdog on 11/18/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "GeneralWeatherCell.h"

static CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

@implementation GeneralWeatherCell

-(void) prepareForReuse {
    self.arrowImageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(0));
}

-(void) rotateArrowToAngel:(NSNumber*) angel {    
    [UIView animateWithDuration: 2.0 delay: 0 options: UIViewAnimationOptionCurveLinear animations:^{
        self.arrowImageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(angel.floatValue));
    } completion:nil];
}

@end
