//
//  BaseViewController.m
//  testTask
//
//  Created by Admin on 11/23/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "BaseViewController.h"

static CGFloat const KBaseControllerStandartAlertDuration = 1.f;

@implementation BaseViewController

-(void) showTemporaryAlertWithMessage:(NSString*) message {
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:message
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:nil];
    [theAlert show];
    [self performSelector:@selector(_dismissAlert:) withObject:theAlert afterDelay:KBaseControllerStandartAlertDuration];
}

-(void) _dismissAlert:(UIAlertView*) alert {
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
    DLog(@"dismissed alert");
}

@end
