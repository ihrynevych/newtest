//
//  GeneralWeatherCell.h
//  testTask
//
//  Created by hotdog on 11/18/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralWeatherCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tempMinLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempMaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UILabel *windSpeedLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

-(void) rotateArrowToAngel:(NSNumber*) angel;
@end
