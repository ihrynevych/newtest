//
//
//  Created by Ihor Hrynevych
//

#import <Foundation/Foundation.h>
#import "CityEntity.h"

static NSString *const kApiClientWeatherUrl = @"http://api.openweathermap.org/data/2.5/weather?q=";
static NSString *const kPlacesSearchUrl = @"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=(cities)&language=en&key=%@";
static NSString *const kPlacesGetInfoByPlaceId = @"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@";
static NSString *const kOpenWeatherAPIKey = @"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f%@";
// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark blocks
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

typedef void (^JSONResponseBlock)(id json, NSError *error);
typedef void (^LeadsResponseBlock)(NSArray *leads, NSError *error);
typedef void (^JSONResponseFailedBlock)(NSError *error, NSString *errorDescription);
typedef void (^URLSessionCompletionBlock)(NSData *data, NSURLResponse *response, NSError *error);

// ----------------------------------------------------------------------------------------------------------------
# pragma mark -
# pragma mark Class interface
# pragma mark -
// ----------------------------------------------------------------------------------------------------------------

@interface LLAPIClient : NSObject  

+ (LLAPIClient *)sharedClient;

-(void) getWeatherFromGPlacesJson:(NSDictionary*) cityJSON withCompletionBlock:(JSONResponseBlock) block;
-(void) getPlacesForCity:(NSString*) city withCompletionBlock:(JSONResponseBlock) block;
-(void) getInfoForPlace:(NSDictionary *) place withCompletionBlock:(JSONResponseBlock) block ;
-(void) addCity:(NSDictionary*) city;
-(NSDictionary*) findCityByName:(NSString*) cityName;
-(void) removeCityByName:(NSString*) cityName;

@property (nonatomic, strong) NSMutableSet *selectedCities;

@end
