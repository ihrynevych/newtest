//
//  UIImageView+URLHelper.h
//  testTask
//
//  Created by hotdog on 11/19/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (URLHelper)

-(void) setImageWithUrl:(NSString*) urlString;

@end
