//
//  CityEntity.m
//  testTask
//
//  Created by hotdog on 11/18/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import "CityEntity.h"

@implementation BaseObject

-(id) initWithJsonObject :(id) json {
    self = [super init];
    if (self && [json isKindOfClass:[NSDictionary class]]) {
        [self fillObjectWithJson:json];
    }
    return self;
}

-(void) fillObjectWithJson:(NSDictionary*) json {}

@end

@implementation CityEntity

-(void) fillObjectWithJson:(NSDictionary *)json {
    self.clouds = [[Clouds alloc] initWithJsonObject:json[@"clouds"]];
    self.coordinates2D = [[Coordinates alloc] initWithJsonObject:json[@"coord"]];
    self.mainProperties = [[Main alloc] initWithJsonObject:json[@"main"]];
    self.weather = [[Weather alloc] initWithJsonObject:json[@"weather"][0]];
    self.city = json[@"city"];
    self.wind = [[Wind alloc] initWithJsonObject:json[@"wind"]];
}

-(void) updateWithFinishBlock:(CityEntityUpdetedBlock) block {
    [API_CLIEENT getWeatherFromGPlacesJson:self.json withCompletionBlock:^(id json, NSError *error) {
        [self.clouds fillObjectWithJson:json[@"clouds"]];
        [self.coordinates2D fillObjectWithJson:json[@"coord"]];
        [self.mainProperties fillObjectWithJson:json[@"main"]];
        [self.weather fillObjectWithJson:json[@"weather"][0]];
        self.city = json[@"city"];
        [self.wind fillObjectWithJson:json[@"wind"]];
        if (block) {
            block ();
        }
    }];
}

@end

@implementation Wind

-(void) fillObjectWithJson:(NSDictionary *)json {
    self.degres = json[@"deg"];
    self.speed = json[@"speed"];
}

@end

@implementation Weather

-(void) fillObjectWithJson:(NSDictionary *)json {
    self.weatherDescription = json[@"description"];
    self.iconUrl = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png", json[@"icon"]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        self.iconImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.iconUrl]]];
    });
}

@end

@implementation Clouds

-(void) fillObjectWithJson:(NSDictionary *)json {
    self.all = json[@"all"];
}

@end

@implementation Coordinates

-(void) fillObjectWithJson:(NSDictionary *)json {
    self.latitude = json[@"lat"];
    self.longtitude = json[@"lon"];
}

@end

@implementation Main

-(void) fillObjectWithJson:(NSDictionary *)json {
    self.groundLevel = json[@"grnd_level"];
    self.humidity = json[@"humidity"];
    self.pressure = json[@"pressure"];
    self.seaLevel = json[@"sea_level"];
    self.currentTemperature = json[@"temp"];
    self.maxTemperature = json[@"temp_max"];
    self.minTemperature = json[@"temp_min"];
}

@end