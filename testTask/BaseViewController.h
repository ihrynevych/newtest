//
//  BaseViewController.h
//  testTask
//
//  Created by Admin on 11/23/15.
//  Copyright © 2015 HotDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void) showTemporaryAlertWithMessage:(NSString*) message;

@end
